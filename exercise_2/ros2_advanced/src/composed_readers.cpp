#include "ros2_advanced/image_reader.hpp"

int main(int argc, char ** argv)
{
  rclcpp::init(argc, argv);

  // Use an executor that runs each node in its own thread
  rclcpp::executors::MultiThreadedExecutor exec{};

  // First node, using default topics
  auto node1 = std::make_shared<ros2_advanced::ImageReader>(
    rclcpp::NodeOptions{}.use_intra_process_comms(true),
    "image_reader1");

  // Second node, reading output of first and republishing it
  auto node2 = std::make_shared<ros2_advanced::ImageReader>(
    rclcpp::NodeOptions{}.use_intra_process_comms(true).parameter_overrides({
      rclcpp::Parameter("input_topic", rclcpp::ParameterValue("/workshop/image")),
      rclcpp::Parameter("output_topic", rclcpp::ParameterValue("/workshop/image2"))
    }),
    "image_reader2");

  exec.add_node(node1);
  exec.add_node(node2);

  exec.spin();

  return 0;
}
