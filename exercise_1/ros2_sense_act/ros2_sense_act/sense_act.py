import math

import rclpy
from rclpy.node import Node
from rcl_interfaces.msg import SetParametersResult
from sensor_msgs.msg import Image, JointState
from mx_joint_controller_msgs.msg import JointCommand


class SenseAct(Node):
    def __init__(self):
        super(SenseAct, self).__init__("sense_act")

        # Step 1: Subscribing to a topic
        # self.init_subscription()

        # Step 2: Publishing to a topic
        # self.init_publisher()

        # Step 4: Timed action
        # self.period = 4
        # self.amplitude = 1
        # self.create_timer(0.02, self.handle_tick)

        # Step 5: Control behavior parameters
        # self.period_param = self.declare_parameter("period", 4.0)
        # self.add_on_set_parameters_callback(self.handle_parameter)

    def init_subscription(self):
        # Create a subscription to joint state information
        self.sub = self.create_subscription(
            None,  # TODO: change to pass message type
            "/topic",  # TODO: change to specify topic name
            lambda msg: None,
            rclpy.qos.qos_profile_system_default,
        )

    def init_publisher(self):
        self.pub = self.create_publisher(
            None,  # TODO: change to pass message type
            "/topic",  # TODO: change to specify topic name
            rclpy.qos.qos_profile_system_default,
        )

        msg = JointCommand()
        # TODO: provide target joint and angle
        self.pub.publish(msg)

    def handle_msg(self, msg):
        # Step 3: Act based on sensor input
        current_head_pan = 0.0  # TODO: get current head pan from message
        msg = JointCommand()
        # TODO: provide target joint and angle
        self.pub.publish(msg)

    def handle_tick(self):
        # Determine current time in seconds
        t = self.get_clock().now().nanoseconds / 1e9

        # Determine oscillation phase and corresponding target angle
        phase = 2 * math.pi * t / self.period
        position = self.amplitude * math.sin(phase)

        # Create and publish control message
        msg = JointCommand()
        msg.name = ["head-pan"]
        msg.position = [position]
        self.pub.publish(msg)

    def handle_parameter(self, parameters):
        all_good = True
        # Go through parameters and find the one we know
        for param in parameters:
            if param.name == "period":
                # Store the value for use in behavior
                self.period = param.value
            else:
                all_good = False

        # Notify whether parameter setting was accepted
        return SetParametersResult(successful=all_good)


def main():
    rclpy.init()

    node = SenseAct()

    rclpy.spin(node)


if __name__ == "__main__":
    main()
